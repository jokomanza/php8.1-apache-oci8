FROM webdevops/php-apache:8.1

COPY src/ app/

# we need libaio!
RUN apt-get update
RUN apt-get install -y libaio1 libaio-dev

ADD instantclient-basic-linux.x64-12.2.0.1.0.zip /tmp/
ADD instantclient-sdk-linux.x64-12.2.0.1.0.zip /tmp/
ADD instantclient-sqlplus-linux.x64-12.2.0.1.0.zip /tmp/
RUN unzip /tmp/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN unzip /tmp/instantclient-sqlplus-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN ln -s /usr/local/instantclient_12_2 /usr/local/instantclient
RUN ln -s /usr/local/instantclient/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so
RUN ln -s /usr/local/instantclient/sqlplus /usr/bin/sqlplus

# set path in environment for additional libraries!
ENV LD_LIBRARY_PATH /usr/local/instantclient_12_2/

# install
RUN echo 'instantclient,/usr/local/instantclient' | pecl install oci8-3.2.1
RUN echo "extension=oci8" > $(pecl config-get ext_dir)/oci8.ini

# don't know if this is necessary
RUN docker-php-ext-enable oci8

#restart apache
RUN service apache2 restart

#show if oci8 is present
RUN php -i | grep oci8

# export ports
EXPOSE 80 443 