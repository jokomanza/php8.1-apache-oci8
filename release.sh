set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=jokomanza
# image name
IMAGE=php-apache-oci8

version=8.1
echo "version: $version"
# run build
./build.sh

docker tag $USERNAME/$IMAGE:latest $USERNAME/$IMAGE:$version
# push it
docker push $USERNAME/$IMAGE:latest
docker push $USERNAME/$IMAGE:$version